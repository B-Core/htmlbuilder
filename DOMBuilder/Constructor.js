(function(){
  this.Constructor = function(params){
    var params = params || {};
    params.width = params.width || 320,
    params.height = params.height || 568,
    params.elements = params.elements || [],
    params.observables = params.Observables || new Observables(),   //nécessite l'inclusion de la lib Observables (Utils)
    params.eventManager = params.eventManager || new EventManager(),    //nécessite la lib customEventManager (Utils)
    params.Timeline = params.Timeline || new animationTimeline(),
    params.animationToolbar = params.animationToolbar || new AnimationToolbar(params || {});

    for(key in params){
      this[key] = params[key];
    }
      
    this.init();
  }
  
  Constructor.prototype.init = function() {
      var workspace = document.createElement("div");
      
      workspace.style.width = this.width + "px";
      workspace.style.height = this.height + "px";
      
      workspace.id = "Workspace";
      
      this.workspace = workspace;
      
      document.body.appendChild(workspace);
      this.elementHandler = new ElementHandler(this);
      
      for(var l = 0; l < this.animationToolbar.tools.length; l++) {
          var el = this.animationToolbar.tools[l];
          for(evt in el) {
              this.eventManager.addListener(el[evt], this[el[evt]], this);
                console.log(el[evt])
              var DOMel = document.getElementById(el[evt]);

              DOMel.addEventListener(evt, function(event){
                  Constructor.eventManager.dispatchEvent(el[evt]);
              });
          }
      }
  }
  
  Constructor.prototype.addElement = function(params) {
    var div = document.createElement("div");
    params = {
        "width": "2em",
        "height": "2em",
        "border": "1px solid lightgrey"
    }

    for(key in params){
      div.style[key] = params[key];
    }
    
    div.id = "element" + this.elements.length;
    div.className = "DOMElement";
      
    this.elements.push(div);
    this.workspace.appendChild(div);
  }
  
  Constructor.prototype.exportCSS = function() {
      var agg = [];
            for(var x = 0; x < this.elements.length; x++) {
               this.Timeline.findAnimation(this.elements[x].id,function(animation){
                    agg.push({
                        style : function(style){
                            var css = {};
                            for(key in style) {
                                if(style[key] !== "" && typeof style[key] !== "function" && key !== "parentRule")css[key] = style[key]
                            }
                            return css;
                        }(animation.element.style),
                        animationString : animation.exportAnimation()
                    });
                });
            }
            console.log(agg);
  }

})();
