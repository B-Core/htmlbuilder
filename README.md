# AnimationParser
CSS Animation Editor

+ click on element to instantiate the animation
+ create animation by adding keys on the timeline
+ select a key to see the properties associated with it

> keyboard

+ SPACE = make animation (parse CSS rules)
+ s = restart animation (withour parsing, add + delete class)
+ r = (while on element) rotate mode
