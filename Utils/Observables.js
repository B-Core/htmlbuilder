(function(){
  this.Observables = function() {
    this.observable = {}
  }

  var Observed = function(data) {
    this.observed = data,
    this.subscribers = []
  }

  Observed.prototype = {
    set : function(data) {
      if(this.observed.nodeName == "INPUT" || this.observed.nodeName == "TEXTAREA") {
        this.observed.value = data;
      } else {
        this.observed = data;
      }
      this.actualize();
      return this.observed;
    },
    get : function() {
      return this.observed;
    },
    actualize : function() {
      for(var x = 0; x < this.subscribers.length; x++) {
        if(this.observed.nodeName == "INPUT" || this.observed.nodeName == "TEXTAREA") {
          this.subscribers[x].call(this.subscribers[x], this.observed.value);
        } else {
          this.subscribers[x].call(this.subscribers[x], this.observed);
        }
      }
    }
  }

  Observables.prototype = {

    register : function(dataName, data) {
      if(!this.observable[dataName]) {
        this.observable[dataName] = new Observed(data);
        var parent = this;
        if(this.observable[dataName].observed.nodeName == "INPUT" || this.observable[dataName].observed.nodeName == "TEXTAREA") {
          this.observable[dataName].observed.addEventListener("input", function(){
            parent.observable[dataName].set(this.value);
          });
        }
      }
      return this;
    },
    subscribe : function(observed, subscribed) {
      if(this.observable[observed].subscribers.indexOf(subscribed) < 0) {
        this.observable[observed].subscribers.push(subscribed);
        if(this.observable[observed].observed.nodeName == "INPUT"|| this.observable[observed].observed.nodeName == "TEXTAREA") {
          this.observable[observed].subscribers[this.observable[observed].subscribers.length-1].call(subscribed, this.observable[observed].observed.value)
        } else {
          this.observable[observed].subscribers[this.observable[observed].subscribers.length-1].call(subscribed, this.observable[observed].observed)
        }
      }
      return this;
    }

  };
})();
