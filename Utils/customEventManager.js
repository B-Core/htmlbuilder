(function(){

  this.EventManager = function() {
    this.listeners = {}
  };
  // place properties here
  // Constructor
  EventManager.prototype = {
      // public methods
      addListener: function(event, fn, parent) {
          if (fn instanceof Function) {
              this.listeners[event] = {fn:fn,parent:parent || window};
          }
          return this;
      },
      dispatchEvent: function(event, params) {
        params = params || {};
//        console.log(this.listeners[event]);
        if(this.listeners[event])this.listeners[event].fn.call(this.listeners[event].parent, params);
        return this;
      },
      removeListener: function(event) {
          if (this.listeners[event]) {
              delete this.listeners[event]
          }
          return this;
      }
  };

})();
