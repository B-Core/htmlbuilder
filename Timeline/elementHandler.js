(function(){
    this.ElementHandler = function(Constructor) {
         function getParentElement(element) {
//            if(element.id == "animationTimeline" || (/paramsBox|confBox/).test(element.className))return false;
             if(element.id !== "Workspace") {
                if(element.parentNode.id == "Workspace") {
                  return true;
                } else if(element.parentNode.id == "animationTimeline") {
                  return false;
                } else {
                  getParentElement(element.parentNode);
                }
             }
          }
          function findAnimation(element) {
            for(var x = 0; x < Constructor.Timeline.animations.length; x++){
              if(Constructor.Timeline.animations[x].animationName == "animation"+count && Constructor.Timeline.animations[x].element == element)return false;
            }
            return true;
          }
          function initEvent(event) {
            event.preventDefault();
            event.stopPropagation();
            Constructor.animationToolbar.target = this;
            var regTrans = new RegExp("([+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?px)", "mg"),
                regRot = new RegExp("([+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?deg)", "mg"),
                strScale = this.style.transform ? this.style.transform.substring(this.style.transform.indexOf("scale(")) : "1,1" || this.style.WebkitTransform ? this.style.WebkitTransform.substring(this.style.WebkitTransform.indexOf("scale(")) : "1,1",
                regScale = new RegExp("[0-9]\\d*(\.\\d+)?", "mg"),
                initX = event.pageX,
                initY = event.pageY,
                initTargetX = this.style.transform ? parseInt(this.style.transform.match(regTrans)[0]) : 0 || this.style.WebkitTransform ? parseInt(this.style.WebkitTransform.match(regTrans)[0]) : 0,
                initTargetY = this.style.transform ? parseInt(this.style.transform.match(regTrans)[1]) : 0 || this.style.WebkitTransform ? parseInt(this.style.WebkitTransform.match(regTrans)[1]) : 0,
                initScaleX = this.style.transform ? parseFloat(strScale.match(regScale)[0]) : 1,
                initScaleY = this.style.transform ? parseFloat(strScale.match(regScale)[1]) : 1,
                initRot = this.style.transform ? parseInt(this.style.transform.match(regRot)[0]) : 0 || this.style.transform ? parseInt(this.style.WebkitTransform.match(regRot)[0]) : 0,
                parent = this,
                el = this.getBoundingClientRect(),
                elWidth = el.width,
                elHeight = el.height,
                elOffsetLeft = el.left,
                elOffsetTop = el.top,
                layerX = (elOffsetLeft+elWidth) - initX,
                layerY = (elOffsetTop+elHeight) - initY,
                invert = {
                  x:false,
                  y:false
                }

            if(Constructor.animationToolbar.mode == "auto") {
              if((layerX > 10 && layerX < elWidth -10) && (layerY > 10 && layerY < elHeight -10)) {
                Constructor.animationToolbar.tool = "move";
              } else if((layerX < 10 || layerX > elWidth -10) && (layerY > 10 && layerY < elHeight - 10)) {
                Constructor.animationToolbar.tool = "resizeX";
              } else if((layerY < 10 || layerY > elHeight -10) && (layerX > 10 && layerX < elWidth - 10)) {
                Constructor.animationToolbar.tool = "resizeY";
              } else if ((layerX < 10 && layerY > elHeight - 10) || (layerX > elWidth - 10 && layerY < 10)) {
                Constructor.animationToolbar.tool = "resize";
              } else if ((layerX < 10 && layerY < 10) || (layerX > elWidth - 10 && layerY > elHeight - 10)) {
                Constructor.animationToolbar.tool = "resize";
              }
            }

            if(layerX < 15 && layerY > elHeight - 15) {
              invert.x = false;
              invert.y = true;
            } else if (layerX > elWidth - 15 && layerY < 15) {
              invert.x = true;
              invert.y = false;
            } else if(layerX > elWidth - 15 || layerY > elHeight - 15) {
              invert.x = true;
              invert.y = true;
            }

            function transform(evt) {
              evt.preventDefault();
              evt.stopPropagation();
              var targetX = initTargetX + (evt.pageX - initX),
                  targetY = initTargetY + (evt.pageY - initY),
                  targetScaleX = invert.x ? initScaleX - ((evt.pageX - initX)/100) : initScaleX + ((evt.pageX - initX)/100),
                  targetScaleY = invert.y ? initScaleY - ((evt.pageY - initY)/100) : initScaleY + ((evt.pageY - initY)/100),
                  targetRot = initRot + parseInt(evt.pageY - initY);

                if(Constructor.animationToolbar.tool == "move") {
                  Constructor.animationToolbar.target.style.transform = "translate("+ targetX +"px,"+ targetY +"px)rotate("+initRot+"deg)scale("+initScaleX+","+initScaleY+")";
                  Constructor.animationToolbar.target.style.WebkitTransform = "translate("+ targetX +"px,"+ targetY +"px)rotate("+initRot+"deg)scale("+initScaleX+","+initScaleY+")";
                } else if (Constructor.animationToolbar.tool == "resizeX") {
                  // console.log(targetScaleX, targetScaleY, initScaleX, initScaleY, strScale.match(regScale));
                  Constructor.animationToolbar.target.style.transform = "translate("+ initTargetX +"px,"+ initTargetY +"px)rotate("+initRot+"deg)scale("+targetScaleX+","+initScaleY+")";
                  Constructor.animationToolbar.target.style.WebkitTransform = "translate("+ initTargetX +"px,"+ initTargetY +"px)rotate("+initRot+"deg)scale("+targetScaleX+","+initScaleY+")";
                } else if(Constructor.animationToolbar.tool == "resizeY") {
                  // console.log(targetScaleX, targetScaleY, initScaleX, initScaleY, strScale.match(regScale));
                  Constructor.animationToolbar.target.style.transform = "translate("+ initTargetX +"px,"+ initTargetY +"px)rotate("+initRot+"deg)scale("+initScaleX+","+targetScaleY+")";
                  Constructor.animationToolbar.target.style.WebkitTransform = "translate("+ initTargetX +"px,"+ initTargetY +"px)rotate("+initRot+"deg)scale("+initScaleX+","+targetScaleY+")";
                } else if (Constructor.animationToolbar.tool == "resize") {
                  Constructor.animationToolbar.target.style.transform = "translate("+ initTargetX +"px,"+ initTargetY +"px)rotate("+initRot+"deg)scale("+targetScaleX+","+targetScaleY+")";
                  Constructor.animationToolbar.target.style.WebkitTransform = "translate("+ initTargetX +"px,"+ initTargetY +"px)rotate("+initRot+"deg)scale("+targetScaleX+","+targetScaleY+")";
                } else if (Constructor.animationToolbar.tool == "rotate") {
                  //console.log(cursor,targetRot, Animationtoolbar.tool);
                  Constructor.animationToolbar.target.style.transform = "translate("+ initTargetX +"px,"+ initTargetY +"px)rotate("+targetRot+"deg)scale("+initScaleX+","+initScaleY+")";
                  Constructor.animationToolbar.target.style.WebkitTransform = "translate("+ initTargetX +"px,"+ initTargetY +"px)rotate("+targetRot+"deg)scale("+initScaleX+","+initScaleY+")";
                } else {
                  console.log("Default");
                }
              //console.log([invert, Animationtoolbar.tool, Animationtoolbar.target]);
              //console.log([evt.pageX, evt.pageY, targetX, targetY, initTargetX, initTargetY]);
            }

            Constructor.workspace.addEventListener("mousemove", transform);
            Constructor.workspace.addEventListener("mouseup", function(){
              for(var t = 0; t < Constructor.Timeline.timelines.length; t++ ) {
                if(Constructor.Timeline.timelines[t].animatedElement == parent.id) {
                  if(Constructor.Timeline.timelines[t].keys.length < 2) {
                    Constructor.Timeline.timelines[t].addKey(Constructor.Timeline.timelines[t].timeline.offsetWidth-15, 100, treatStyle(parent.style));
                    } else {
                      var nodeList = Array.prototype.slice.call( Constructor.Timeline.timelines[t].timeline.children );
                      var selectedKey = Constructor.Timeline.timelines[t].keys[nodeList.indexOf(Constructor.Timeline.selected)] || Constructor.Timeline.timelines[t].keys[Constructor.Timeline.timelines[t].keys.length-1];
                      selectedKey.keyText = selectedKey.keyText;
                      selectedKey.parameters = treatStyle(parent.style);
                    }
                }
                Constructor.animationToolbar.target = null;
              }
              Constructor.workspace.removeEventListener("mousemove", transform);
              Constructor.workspace.removeEventListener("mouseup", arguments.callee);
            });

          }
          Constructor.workspace.addEventListener("mousedown", function(event){
            var target = event.target;
            count = typeof count == "undefined" ? 0 : count;
              
            if(count == 0) {
              Constructor.workspace.addEventListener("mousemove", handleCursor);
            }
              
            if(findAnimation(target)){
              if(getParentElement(target) && target.id !== "Workspace" && target !== "BODY"){
                count++;
                console.log("init Event on ",target);
                target.addEventListener("mousedown", initEvent);
                console.log("new AnimationParser");
                new AnimationParser(target, "animation"+count, Constructor.Timeline, {direction:"normal",iterationCount:"1", fillMode:"none"});
                console.log("add key to 0%");
                for(t = 0; t < Constructor.Timeline.timelines.length; t++ ) {
                  if(Constructor.Timeline.timelines[t].animatedElement == target.id) {
                    target.style.transform = "translate(0,0)rotate(0deg)scale(1,1)";
                    target.style.WebkitTransform = "translate(0,0)rotate(0deg)scale(1,1)";
                    Constructor.Timeline.timelines[t].addKey(0, 0, treatStyle(target.style));
                  }
                }
              }
            }
          });
          var handleCursor = function(evt){
            var target = evt.target;
            if(Constructor.Timeline.findAnimation(target.id)) {
              var el = target.getBoundingClientRect(),
                  elWidth = el.width,
                  elHeight = el.height,
                  elOffsetLeft = el.left,
                  elOffsetTop = el.top,
                  mouseX = evt.pageX,
                  mouseY = evt.pageY,
                  layerX = (elOffsetLeft+elWidth) - mouseX,
                  layerY = (elOffsetTop+elHeight) - mouseY;

              switch(Constructor.animationToolbar.mode) {
                case "auto":
                  if((layerX > 10 && layerX < elWidth -10) && (layerY > 10 && layerY < elHeight -10)) {
                    Constructor.workspace.style.cursor = "move";
                  } else if((layerX < 10 || layerX > elWidth -10) && (layerY > 10 && layerY < elHeight - 10)) {
                    Constructor.workspace.style.cursor = "ew-resize";
                  } else if((layerY < 10 || layerY > elHeight -10) && (layerX > 10 && layerX < elWidth - 10)) {
                    Constructor.workspace.style.cursor = "ns-resize";
                  } else if ((layerX < 10 && layerY > elHeight - 10) || (layerX > elWidth - 10 && layerY < 10)) {
                    Constructor.workspace.style.cursor = "nesw-resize";
                  } else if ((layerX < 10 && layerY < 10) || (layerX > elWidth - 10 && layerY > elHeight - 10)) {
                    Constructor.workspace.style.cursor = "nwse-resize";
                  }
                  break;
                case "tool":
                  if (Constructor.animationToolbar.tool == "rotate") {
                    Constructor.workspace.style.cursor = "url(Timeline/images/rotate.gif), auto";
                  } else {
                    dConstructor.workspace.style.cursor = "default";
                  }
                  break;
                default:
              }
              //console.log([evt, "elementWidth:",elWidth, "elementHeight:",elHeight,"elementHeight:",elOffsetLeft,"elementTop:",elOffsetTop,"mouseX:", mouseX,"mouseY:", mouseY,"layerX:", layerX,"layerY:", layerY]);
            } else {
              Constructor.workspace.style.cursor = "default";
            }
          }
        
    }
 
})()
