(function(){
  this.AnimationToolbar = function(params) {
    params = params || {},
    params.tools = params.tools || [],
    params.target = null,
    params.mode = "auto",
    params.tool = "auto";
      
    for(key in params){
      this[key] = params[key];
    }
  }
})()
