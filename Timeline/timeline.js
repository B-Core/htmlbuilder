function treatStyle(elementStyle) {
  var st = {}
  for(var i = 0; i < elementStyle.length; i++) {
    st[elementStyle[i]] = elementStyle[elementStyle[i]];
  }
  return st;
}
(function(){
  this.Timeline = function(element, container, title, timeline) {
    this.animatedElement = element,
    this.container = container,
    this.title = title,
    this.parent = Constructor.Timeline,
    this.timeline = timeline,
    this.keys = [],
    this.addKey = function(left, keyText, parameters) {
      var displayKey = document.createElement("div"),
          regStyle = new RegExp("(\\d+)", "mg"),
          divAnimatedElement = document.getElementById(this.animatedElement),
          percent = 0 || keyText,
          parameters = parameters.length == undefined ? treatStyle(document.getElementById(this.animatedElement).style) : parameters,
          parent = this;

      displayKey.className = "key";
      displayKey.style.left = left + "px";
      var key = new Key(left, displayKey, keyText+"%", parameters);
      key.paramsBox = new ParamsBox((this.timeline.offsetParent.offsetTop-170),left, key);

      var initEvent = function() {
        event.preventDefault();
        event.stopPropagation();
        var displayEl = this;
        var nodeList = Array.prototype.slice.call( parent.timeline.children );

        if(parent.parent.selected == this) {
          divAnimatedElement.style.cssText = parent.keys[nodeList.indexOf(this)].getCssText();
        } else {
          if(typeof parent.parent.selected !== "undefined")parent.parent.selected.style.border = "";
          this.style.border = "1px solid rgb(56, 121, 153)";
          divAnimatedElement.style.cssText = parent.keys[nodeList.indexOf(this)].getCssText();
          parent.parent.selected = this;
        }

        var move = function() {
          event.preventDefault();
          event.stopPropagation();
          if(event.target == parent.timeline && event.layerX <= (parent.timeline.offsetWidth-displayEl.offsetWidth)) {
            percent = Math.floor((event.layerX*100)/(parent.timeline.offsetWidth-20));
            displayEl.style.left = event.layerX + "px";
            if(event.layerX > 30)parent.findKey(key).paramsBox.container.style.left = (event.pageX-150) + "px";
          }
        }

        window.addEventListener("mousemove", move);
        window.addEventListener("mouseup", function(){
          parent.actualizeKeys(key, percent, key.parameters);
          //console.log(parent,key, percent);
          window.removeEventListener("mousemove", move);
          window.removeEventListener("mouseup", arguments.callee);
        });
      }

      var displayParamsBox = function() {
          toggle = typeof toggle == "undefined" ? false : toggle;
          event.preventDefault();
          event.stopPropagation();
          parent.findKey(key).paramsBox.container.style.display = toggle ? "none" : "block";
          toggle = !toggle;
      }

      displayKey.addEventListener("dblclick", displayParamsBox);
      displayKey.addEventListener("mousedown", initEvent);
      this.keys.push(key);
      this.actualize();

      if(this.keys.length > 2 && this.keys[this.keys.length-1].keyText == "100%") {
        var beforeElement = this.keys[(this.keys.indexOf(key))+1].displayElement;
        this.timeline.insertBefore(displayKey, beforeElement);
      } else {
        this.timeline.appendChild(displayKey);
      }
      // console.log(key,this.keys);

    },
    this.findKey = function(key) {
      return this.keys[this.keys.indexOf(key)]
    },
    this.actualize = function() {
      this.keys.sort(function(a, b) {
        return parseInt(a.keyText) - parseInt(b.keyText);
      });
    },
    this.actualizeKeys = function(key, keyText, properties) {
      var key = this.findKey(key);
      key.keyText = keyText + "%";
      key.parameters = properties;
      this.keys.sort(function(a, b) {
        return parseInt(a.keyText) - parseInt(b.keyText);
      });
    },
    this.keysToSteps = function(){
      if(this.keys.length >= 2) {
        var that = this;
        this.parent.findAnimation(this.animatedElement,function(animation){
          var steps = [];
           //console.log(animation);
          for(var k = 0; k < that.keys.length; k++) {
            steps.push(that.keys[k]);
          }
          //console.log(steps);
          animation.addStep({steps});
        });
      } else {
        console.log("not enough keyframes, minimum 2");
      }
    },
    this.checkKeyEvent = function(event) {
      var keypress = event.charCode;

      switch(keypress) {
        case 32 :
          var dispEl = document.getElementById(this.animatedElement);
          dispEl.removeAttribute("style");
          this.keysToSteps();
          break;
        case 115 :
          this.parent.findAnimation(this.animatedElement,function(animation){
            animation.restart();
          });
          break;
        default :
      }
    }
    this.keyboardEvent = function() {
      var parent = this;
      return window.addEventListener("keypress", function(event){
        parent.checkKeyEvent(event);
      });
    }
    this.keyboardEvent();
  }

  var Key = function(left, displayElement, keyText, parameters){
    this.left = left,
    this.displayElement = displayElement,
    this.keyText = keyText,
    this.parameters = parameters,
    this.getCssText = function() {
      var str = "";
      for(prop in this.parameters) {
        str += prop+":"+this.parameters[prop]+";";
      }
      //console.log(str);
      return str;
    }
  }

  var ParamsBox = function(top, left, key){
    var container = document.createElement("div");
    var create = document.createElement("div");
    var del = document.createElement("div");
    var apply = document.createElement("div");
    var parent = this;
    this.CSSProperties = [];
    create.className = "createParams";
    del.className = "removeParams";
    apply.className = "applyParams";

    container.className = "paramsBox";
    container.style.top = top + "px";
    container.style.left = left + "px";
    container.style.display = "none";
    create.innerHTML = "<p>+</p>";
    del.innerHTML = "<p>-</p>";
    apply.innerHTML = "<p>Apply</p>";

    this.key = key;
    container.appendChild(create);
    container.appendChild(del);
    container.appendChild(apply);
    this.container = container;
    document.body.appendChild(container);

    create.addEventListener("click", function(){
      parent.createField();
    });

    del.addEventListener("click", function(){
      parent.removeField();
    });

    apply.addEventListener("click", function(){
      parent.changeKeyParameter();
    });

    this.createField = function(){
      var cont = document.createElement("div");
      cont.className = "inputCont";

      var select = document.createElement("select");
      var field = document.createElement("input");
      field.type = "text";

      var optionText = ["width","height","opacity","background-color","background-position","background-size","box-shadow",
      "border","border-radius","padding","margin","left","right","top","bottom","display","outline","outline-offset",
      "color","font-size","letter-spacing","word-spacing","font-weight","line-height"];
      for(var x = 0; x < optionText.length; x++) {
        var option = document.createElement("option");
        option.text = optionText[x];
        select.add(option);
      }

      cont.appendChild(select);
      cont.appendChild(field);
      this.CSSProperties.push(new CSSPropertie(select, field));
      this.container.appendChild(cont);
    },

    this.removeField = function() {
      var fields = this.container.children;
      var el = this.container.childNodes[fields.length-1];
      if(el.className == "inputCont")this.container.removeChild(el);
      this.CSSProperties.pop();
    },

    this.changeKeyParameter = function() {
      for(var x = 0; x < this.CSSProperties.length; x++) {
        this.key.parameters[this.CSSProperties[x].CSSProp] = this.CSSProperties[x].CSSValue;
      }
      //console.log(this);
    }

    var CSSPropertie = function(select, field) {
      this.CSSProp = select.value,
      this.CSSValue = field.value,
      this.HTMLSelect = select,
      this.HTMLInput = field,
      this.init = function() {
        var parent = this;
        var checkSelectValue = function() {
          return parent.CSSProp = this.value
        }
        var checkFieldValue = function() {
          return parent.CSSValue = this.value
        }
        this.HTMLSelect.addEventListener("change", checkSelectValue);
        this.HTMLInput.addEventListener("keyup", checkFieldValue);
      }
      this.init();
    }

  }

})()
