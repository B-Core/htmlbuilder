(function(){
  this.AnimationParser = function(element, animationName, timeline, params){
    if(!animationName)throw "You must give an animation name"
    this.element = element,
    this.animationStyle = {
      duration: params.duration || 1,
      delay: params.delay || 0.1,
      direction: params.direction || "normal",
      fillMode: params.fillMode || "both",
      iterationCount: params.iterationCount || 1,
      timing: params.timing || "normal"
    },
    this.timeline = timeline,
    this.keyframes = [],
    this.percentCount = 0,
    this.currentPercent = 0,
    this.init = function() {
      var domPrefixes = 'Webkit'.split(' ');
      this.animationString = ["animation"];
      this.keyframeprefix = [""];
      this.animationName = animationName;

      for( var i = 0; i < domPrefixes.length; i++ ) {
        var pfx = domPrefixes[ i ];
        this.animationString.push(pfx + 'animation');
        this.keyframeprefix.push('-' + pfx.toLowerCase() + '-');
      }

      this.keyframes = makeArrayKeyframes(findKeyframesRule(this.animationName));
      this.timeline.actualize(this);
      this.ConfigBox = new ConfigBox(this.animationName, this.animationStyle, this);
      return animationName;
    },
    this.animationName = this.init(),
    this.setAnimationEvent = function() {
      var parent = this;
      var element = this.element;
      // webkit events
      element.addEventListener("webkitAnimationStart", function(){
        onAnimationStart.call(parent);
      });
      element.addEventListener("webkitAnimationIteration", function(){
        onAnimationIteration.call(parent);
      });
      element.addEventListener("webkitAnimationEnd", function(){
        onAnimationEnd.call(parent);
      });

      // standard events
      element.addEventListener("animationstart", function(){
        onAnimationStart.call(parent);
      });
      element.addEventListener("animationiteration", function(){
        onAnimationIteration.call(parent);
      });
      element.addEventListener("animationend", function(){
        onAnimationEnd.call(parent);
      });
    },
    this.deleteStep = function(params,callback) {
      var refresh = false || params.refresh;
      var percent;
      //console.log(refresh, steps)
      var CSSanimation = findKeyframesRule(this.animationName,true);
      if(params.steps) {
        for(var x = 0; x < CSSanimation.length; x++) {
          for(var y = 0; y < this.keyframes.length; y++) {
            CSSanimation[x].deleteRule(CSSanimation[x].cssRules[0].keyText);
          }
        }
        for(var step = 0; step < params.steps.length; step++) {
          this.keyframes.splice(params.steps[step], 1);
        }
        this.keyframes.sort(function(a, b) {
          return parseInt(a.keyText) - parseInt(b.keyText);
        });
        percent = 100/(this.keyframes.length-1);
        for(var r = 0; r < this.keyframes.length; r++) {
          this.keyframes[r].keyText = Math.floor(percent*r) + "%";
        }
        for(var x = 0; x < CSSanimation.length; x++) {
          for(var y = 0; y < this.keyframes.length; y++) {
            CSSanimation[x].appendRule(this.keyframes[y].keyText + "{" + this.keyframes[y].parameters + "}");
          }
        }
      } else if(params.keys) {
        for(var x = 0; x < CSSanimation.length; x++) {
          for(var y = 0; y < params.keys.length; y++) {
            CSSanimation[x].deleteRule(params.keys[y]);
          }
        }
        this.keyframes = makeArrayKeyframes(findKeyframesRule(this.animationName));
      } else {
        for(var x = 0; x < CSSanimation.length; x++) {
          for(var y = 0; y < this.keyframes.length; y++) {
            CSSanimation[x].deleteRule(CSSanimation[x].cssRules[0].keyText);
          }
        }
        this.keyframes = makeArrayKeyframes(findKeyframesRule(this.animationName));
      }

      if(refresh)this.keyframes = makeArrayKeyframes(findKeyframesRule(this.animationName));
      if(!callback){
        this.restart();
      }

      if(callback)callback();
      // console.log(this, findKeyframesRule(this.animationName))
    },
    this.addStep = function(params) {
      deleteStylesheetRule(this.element);
      addStylesheetRules(this.element,[
        ["."+this.animationName,
          [this.animationString[0]+"-name", animationName],
          [this.animationString[0]+"-duration", this.animationStyle.duration+"s"],
          [this.animationString[0]+"-direction", this.animationStyle.direction],
          [this.animationString[0]+"-delay", this.animationStyle.delay+"s"],
          [this.animationString[0]+"-fill-mode", this.animationStyle.fillMode],
          [this.animationString[0]+"-iteration-count", this.animationStyle.iterationCount],
          [this.animationString[0]+"-timing-function", this.animationStyle.timing],
          [this.animationString[1]+"-name", animationName],
          [this.animationString[1]+"-duration", this.animationStyle.duration+"s"],
          [this.animationString[1]+"-direction", this.animationStyle.direction],
          [this.animationString[1]+"-delay", this.animationStyle.delay+"s"],
          [this.animationString[1]+"-fill-mode", this.animationStyle.fillMode],
          [this.animationString[1]+"-iteration-count", this.animationStyle.iterationCount],
          [this.animationString[1]+"-timing-function", this.animationStyle.timing]
        ]
      ]);
      var percent = 100/(this.keyframes.length+params.steps.length-1);
      var CSSanimation = findKeyframesRule(this.animationName, true);
      var parent = this;
      this.deleteStep({refresh: false},function(){
        for(var st = 0; st < params.steps.length; st++) {
          var rule = new Object();
          rule.parameters = params.steps[st].getCssText();
          rule.keyText = params.steps[st].keyText || percent*(parent.keyframes.length-1);
          parent.keyframes.push(rule);
        }
        parent.keyframes.sort(function(a, b) {
          return parseInt(a.keyText) - parseInt(b.keyText);
        });
        if(!params.steps[0].keyText) {
          for(var r = 0; r < parent.keyframes.length; r++) {
            parent.keyframes[r].keyText = Math.floor(percent*r) + "%";
          }
        }
        if(CSSanimation.length == 0) {
          for(var ss = 0; ss < document.styleSheets.length; ss++) {
            for(var kpref = 0; kpref < parent.keyframeprefix.length; kpref++) {
              var keyFrame = '@' + parent.keyframeprefix[kpref] + 'keyframes '+ parent.animationName +'{ '+' }';
              document.styleSheets[ss].insertRule( keyFrame, 0 );
            }
          }
          CSSanimation = findKeyframesRule(parent.animationName, true);
          for(var x = 0; x < CSSanimation.length; x++) {
            for(var y = 0; y < parent.keyframes.length; y++) {
              CSSanimation[x].appendRule(parent.keyframes[y].keyText + "{" + parent.keyframes[y].parameters + "}");
            }
          }
        } else {
          for(var x = 0; x < CSSanimation.length; x++) {
            for(var y = 0; y < parent.keyframes.length; y++) {
              CSSanimation[x].appendRule(parent.keyframes[y].keyText + "{" + parent.keyframes[y].parameters + "}");
            }
          }
        }
        parent.restart();
        //console.log(parent.keyframes, findKeyframesRule(parent.animationName));
      });
    },
    this.pause = function(){
      this.element.style["animation-play-state"] = "paused";
      this.element.style["-webkit-animation-play-state"] = "paused";
    },
    this.play = function(){
      this.element.style["animation-play-state"] = "running";
      this.element.style["-webkit-animation-play-state"] = "running";
    },
    this.restart = function() {
      var parent = this;
      this.element.classList.remove(this.animationName);
      window.setTimeout(function(){
        parent.element.classList.add(parent.animationName);
      },100);
    },
    this.exportAnimation = function() {
      var parent = this;
      var style = [];
      var st = function(){
        var s = document.head.getElementsByTagName("style");
        for(var x = 0; x < s.length; x++) {
          if(s[x].getAttribute("name"))return s[x].sheet;
        }
      }();
      for(var j = 0; j < st.cssRules.length;j++) {
        if(st.cssRules[j].name == this.animationName || st.cssRules[j].selectorText == "#"+this.element.id) {
          style.push(st.cssRules[j].cssText);
        }
      }
      return style;
    }
  }

  var ConfigBox = function(animationName,params, parent) {
    var directions = ["normal","reverse","alternate","alternate-reverse"],
        fillModes = ["none","forwards","backwards","both"],
        timings = ["linear","ease","ease-in","ease-out","ease-in-out"];

    var container = document.createElement("div");
    var toggleParameters = document.createElement("div");
    var title = document.createElement("h3");
    this.timeline = parent.timeline.getTimeline(parent.element.id);
    this.parent = parent;
    var parent = this;

    container.className = "confBox";
    toggleParameters.className = "toggleParameters";

    toggleParameters.innerHTML = "<p>&#187;</p>"
    title.innerHTML = animationName;

    container.appendChild(title);

    for(prop in params){
      var label = document.createElement("label");
      var confContainer = document.createElement("div");

      confContainer.className = "confContainer";

      label.innerHTML = prop + ": ";
      if(prop == "direction") {
        createSelect(directions);
      } else if (prop == "fillMode") {
        createSelect(fillModes);
      } else if (prop == "timing") {
        createSelect(timings);
      } else {
        var input = document.createElement("input");
        input.className = prop;
        input.value = params[prop];
        confContainer.appendChild(label);
        confContainer.appendChild(input);

        input.addEventListener("change", checkValue);
      }

      container.appendChild(confContainer);
    }

    function createSelect(array) {
      var select = document.createElement("select");
      select.className = prop;
      for(var x = 0; x < array.length; x++) {
        var option = document.createElement("option");
        option.text = array[x];
        select.add(option);
      }
      confContainer.appendChild(label);
      confContainer.appendChild(select);

      select.addEventListener("change", checkValue);
    }

    toggleParameters.addEventListener("click", toggleParam);

    function checkValue() {
      parent.parent.animationStyle[this.className] = this.value;
    }

    function toggleParam() {
      parent.timeline.container.style.height = parent.timeline.container.style.height == "200px" ? "42px" : "200px";
    }

    this.container = container;
    // console.log(this.timeline);
    this.timeline.title.appendChild(toggleParameters);
    this.timeline.title.appendChild(container);
  }

  var formatedDate = function(timestamp){
    // create a new javascript Date object based on the timestamp
    var date = timestamp !== "undefined" ? new Date(timestamp) : new Date();
    // hours part from the timestamp
    var hours = date.getHours();
    // minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // seconds part from the timestamp
    var seconds = "0" + date.getSeconds();

    // will display time in 10:30:23 format
    var formattedTime = {
      hours: hours,
      minutes: parseInt(minutes.substr(-2)),
      seconds: parseInt(seconds.substr(-2))
    };
    return formattedTime
  }

  var findKeyframesRule = function(rule, both) {
    var ss = document.styleSheets;
    var keyframes = [];
    for(var s = 0; s < ss.length; s++) {
      if(ss[s].cssRules !== null) {
        for (var j = 0; j < ss[s].cssRules.length; j++) {
          if (ss[s].cssRules[j].type == window.CSSRule.WEBKIT_KEYFRAMES_RULE &&
          ss[s].cssRules[j].name == rule || ss[s].cssRules[j].type == window.CSSRule.MOZ_KEYFRAMES_RULE &&
          ss[s].cssRules[j].name == rule || ss[s].cssRules[j].type == window.CSSRule.KEYFRAMES_RULE &&
          ss[s].cssRules[j].name == rule) {
            if(!both)return ss[s].cssRules[j];
            keyframes.push(ss[s].cssRules[j]);
          }
        }
      }
    }
    return keyframes;
  }

  var makeArrayKeyframes = function(keyframes) {
    var keyframeArray = [];
      //parse the property of animation
      var parseBracket = function(text) {
        var openBracket = [];
        for(var pos = 0; pos < text.length; pos++) {
          if(text[pos] == "{" || text[pos] == "}")openBracket.push(pos);
        }
        return text.substring(openBracket[0], openBracket[1]).replace("{", "");
      }
      // Makes an array of the current percent values
      // in the animation
      if(keyframes.length > 0) {
        for(var i = 0; i < keyframes.cssRules.length; i ++) {
          var prop = parseBracket(keyframes.cssRules[i].cssText);
          var key = keyframes.cssRules[i].keyText.replace('%', '');
          var obj = new Object();
          obj.parameters = prop;
          obj.keyText = keyframes.cssRules[i].keyText;
          keyframeArray.push(obj);
        }
      }
    return keyframeArray;
  }

  var addStylesheetRules = function (element, rules) {
    var styleEl = document.createElement('style'),
        styleSheet;

    styleEl.setAttribute("name",element.id);
    // Append style element to head
    document.head.appendChild(styleEl);

    // Grab style sheet
    styleSheet = styleEl.sheet;

    for (var i = 0, rl = rules.length; i < rl; i++) {
      var j = 1, rule = rules[i], selector = rules[i][0], propStr = '';
      // If the second argument of a rule is an array of arrays, correct our variables.
      if (Object.prototype.toString.call(rule[1][0]) === '[object Array]') {
        rule = rule[1];
        j = 0;
      }

      for (var pl = rule.length; j < pl; j++) {
        var prop = rule[j];
        propStr += prop[0] + ':' + prop[1] + (prop[2] ? ' !important' : '') + ';\n';
      }

      // Insert CSS Rule
      styleSheet.insertRule(selector + '{' + propStr + '}', styleSheet.cssRules.length);
    }
  }

  var deleteStylesheetRule = function(element) {
    var styles = document.head.getElementsByTagName("style");
    if(styles){
      for(var e = 0; e < styles.length; e++) {
        if(styles[e].getAttribute("name") == element.id)document.head.removeChild(styles[e]);
      }
    } else {
      console.log("no styles");
    }
  }

  var onAnimationStart = function() {
    var animationDuration = parseInt(getComputedStyle(this.element)["animationDuration"].replace("s", ""));
    var parent = this;
    clearInterval(this.percentCount);
    this.percentCount = window.setInterval(function() {
        if (parent.currentPercent < 100) {
          parent.currentPercent += 1;
        } else {
          parent.currentPercent = 0;
        }
        console.log(parent.currentPercent);
      }, animationDuration*10);
      //console.log(formatedDate(event.timeStamp));
      console.log("start", event);
    }

  var onAnimationIteration = function() {
    var animationDuration = parseInt(getComputedStyle(this.element)["animationDuration"].replace("s", ""));
    this.currentPercent = 0;
    var parent = this;
    this.elapsedTime = event.elapsedTime;
    clearInterval(this.percentCount);
    this.percentCount = window.setInterval(function() {
        if (parent.currentPercent < 100) {
          parent.currentPercent += 1;
        } else {
          parent.currentPercent = 0;
        }
        // console.log(parent.currentPercent);
      }, animationDuration*10);
    console.log("iteration",event);
  }

  var onAnimationEnd = function() {
    clearInterval(this.percentCount);
    console.log(event.elapsedTime);
    console.log("end", event);
  }

}())
