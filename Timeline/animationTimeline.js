(function(){
  this.animationTimeline = function(){
    this.animations = [],
    this.timelines = [],
    this.init = function() {
      this.element = document.createElement("div");
      this.close = document.createElement("button");
      var parent = this,
          frag = document.createDocumentFragment();

      this.element.id = "animationTimeline";
      this.close.className = "close";
      this.close.innerHTML = "^";

      this.close.addEventListener("click", function(){
        toggle = typeof toggle == "undefined" ? true : toggle;
        if(toggle) {
          parent.element.style.WebkitTransform = "translate(0,0)";
          parent.element.style.transform = "translate(0,0)";
          this.style.WebkitTransform = "rotate(180deg)";
          this.style.transform = "rotate(180deg)";
        } else {
          parent.element.style.WebkitTransform = "translate(0,225px)";
          parent.element.style.transform = "translate(0,225px)";
          this.style.WebkitTransform = "rotate(0deg)";
          this.style.transform = "rotate(0deg)";
        }
        toggle = !toggle;
      });
      this.element.appendChild(this.close);

      frag.appendChild(this.element);

      document.body.appendChild(frag);
//      this.keyboardEvent();
      console.log("Instanciating timeline");
    },
    this.initEvent = function(time) {
      var parent = this;
      var click = function(e) {
        e.preventDefault();
        var animatedElement = this.getAttribute("data-element");
        var percent = Math.floor((e.offsetX*100)/this.offsetWidth);
        for(var t = 0; t < parent.timelines.length; t++) {
          if(parent.timelines[t].animatedElement == animatedElement) {
            parent.timelines[t].addKey(e.layerX,percent,{});
          }
        }
      }
      time.addEventListener("mousedown", click);
    },
    this.findAnimation = function(animatedElement, callback) {
      for(var a = 0; a < this.animations.length; a++) {
        if(this.animations[a].element.id == animatedElement) {
          if(callback)callback.call(this, this.animations[a]);
          return true;
          //console.log(this.parent.animations[a], steps);
        }
      }
      return false;
    },
    this.getTimeline = function(animatedElementId) {
      for(var t = 0; t < this.timelines.length; t++) {
        console.log(this.timelines[t].animatedElement)
        if(this.timelines[t].animatedElement == animatedElementId)return this.timelines[t];
      }
      console.log("no timeline associated");
      return false;
    }
    this.actualize = function(animation) {
      this.animations.push(animation);

      var frag = document.createDocumentFragment();
      var animatedElement = document.createElement("div");
      var animationTitle = document.createElement("div");
      var time = document.createElement("div");

      this.timelines.push(new Timeline(animation.element.id, animatedElement, animationTitle, time));

      animatedElement.className = "animatedElement";
      animationTitle.className = "animationTitle";
      time.setAttribute("data-element", animation.element.id);
      time.className = "time";

      animationTitle.innerHTML = "<p>"+animation.element.id+"</p>";

      animatedElement.appendChild(animationTitle);
      animatedElement.appendChild(time);
      frag.appendChild(animatedElement);

      this.element.appendChild(frag);
      this.initEvent(time);
    },
    this.checkKeyEvent = function(event) {
      var keypress = event.charCode;

      switch(keypress) {
        case 114 :
          Constructor.animationToolbar.mode = Constructor.animationToolbar.mode == "tool" ? "auto" : "tool";
          Constructor.animationToolbar.tool = Constructor.animationToolbar.tool == "rotate" ? "auto" : "rotate";
          console.log(Constructor.animationToolbar.tool);
          break;
        case 109 :
          break;
        case 101 :
            Constructor.exportCSS();
            break;  
        default :
          console.log(keypress);
      }
    }
    this.keyboardEvent = function() {
      var parent = this;
      return window.addEventListener("keypress", function(event){
        parent.checkKeyEvent(event);
      });
    },
    this.init()
  }
}())
